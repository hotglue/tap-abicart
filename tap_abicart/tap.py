"""Abicart tap class."""

from typing import List

from singer_sdk import Stream, Tap
from singer_sdk import typing as th

from tap_abicart.streams import (
    ArticleGroupStream,
    ArticlesStream,
    OrderItemStream,
    OrdersStream,
    SuppliersGroupStream,
    WebshopStream,
)

STREAM_TYPES = [
    ArticlesStream,
    ArticleGroupStream,
    SuppliersGroupStream,
    OrdersStream,
    OrderItemStream,
    WebshopStream,
]


class TapAbicart(Tap):
    """AbiCart tap class."""

    name = "tap-abicart"

    # TODO: Update this section with the actual config values you expect:
    config_jsonschema = th.PropertiesList(
        th.Property("username", th.StringType, required=True),
        th.Property("password", th.StringType, required=True),
        th.Property("webshop", th.StringType, required=True),
        th.Property(
            "api_url",
            th.StringType,
            default="https://admin.abicart.se/backend/jsonrpc/v1/",
            description="The url for the API service",
        ),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == "__main__":
    TapAbicart.cli()
