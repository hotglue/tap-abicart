"""Stream type classes for tap-abicart."""

from typing import Any, Optional

import requests
from singer_sdk import typing as th

from tap_abicart.client import AbicartStream


class ArticlesStream(AbicartStream):
    """Define custom stream."""

    name = "articles"
    method = "Article.list"
    path = ""
    rest_method = "POST"
    primary_keys = ["uid"]
    replication_key = "changed"
    schema = th.PropertiesList(
        th.Property("uid", th.NumberType),
        th.Property("created", th.DateTimeType),
        th.Property("changed", th.DateTimeType),
        th.Property("articleNumber", th.StringType),
        th.Property("name", th.CustomType({"type": ["object", "string"]})),
        th.Property("description", th.CustomType({"type": ["object", "string"]})),
        th.Property("introductionText", th.CustomType({"type": ["object", "string"]})),
        th.Property("freeFreight", th.BooleanType),
        th.Property("isBulky", th.BooleanType),
        th.Property(
            "price",
            th.ObjectType(
                th.Property("current", th.CustomType({"type": ["object", "string"]})),
                th.Property("regular", th.CustomType({"type": ["object", "string"]})),
                th.Property(
                    "specialOffer", th.CustomType({"type": ["object", "string"]})
                ),
            ),
        ),
        th.Property("showPricesIncludingVat", th.BooleanType),
        th.Property(
            "pricing", th.ArrayType(th.CustomType({"type": ["object", "string"]}))
        ),
        th.Property("url", th.CustomType({"type": ["object", "string"]})),
        th.Property("baseName", th.CustomType({"type": ["object", "string"]})),
        th.Property(
            "stock",
            th.ObjectType(
                th.Property("show", th.BooleanType),
                th.Property("useStock", th.BooleanType),
                th.Property("stock", th.NumberType),
                th.Property("hideUnstocked", th.BooleanType),
                th.Property("hideUnstockedChoices", th.BooleanType),
                th.Property("message", th.CustomType({"type": ["object", "string"]})),
            ),
        ),
        th.Property("draft", th.BooleanType),
        th.Property("hidden", th.BooleanType),
        th.Property("isBuyable", th.BooleanType),
        th.Property("deliveryInfo", th.CustomType({"type": ["object", "string"]})),
        th.Property("images", th.ArrayType(th.StringType)),
        th.Property(
            "imagesAltText",
            th.ArrayType(
                th.ObjectType(
                    th.Property("image", th.StringType),
                    th.Property(
                        "altText", th.CustomType({"type": ["object", "string"]})
                    ),
                )
            ),
        ),
        th.Property("articleTemplate", th.NumberType),
        th.Property("attributes", th.CustomType({"type": ["object", "string"]})),
        th.Property("sortedAttributes", th.ArrayType(th.NumberType)),
        th.Property("choiceSchema", th.CustomType({"type": ["object", "string"]})),
        th.Property("variants", th.ArrayType(th.CustomType({"type": ["object", "string"]}))),
        th.Property("articlegroup", th.NumberType),
        th.Property("showInArticlegroups", th.ArrayType(th.NumberType)),
        th.Property("weight", th.NumberType),
        th.Property("textAfterCheckout", th.CustomType({"type": ["object", "string"]})),
        th.Property("vatRate", th.NumberType),
        th.Property("hasChoices", th.BooleanType),
        th.Property(
            "choices", th.ArrayType(th.NumberType)
        ),
        th.Property("choiceOptions", th.CustomType({"type": ["object", "string"]})),
        th.Property(
            "choiceOptionPrices", th.CustomType({"type": ["object", "string"]})
        ),
        th.Property("presentationOnly", th.BooleanType),
        th.Property("news", th.BooleanType),
        th.Property("ean", th.StringType),
        th.Property("sku", th.StringType),
        th.Property("pageTitle", th.CustomType({"type": ["object", "string"]})),
        th.Property("metaDescription", th.CustomType({"type": ["object", "string"]})),
        th.Property("metaKeywords", th.CustomType({"type": ["object", "string"]})),
        th.Property("vatIsIncluded", th.BooleanType),
        th.Property(
            "orderedWithArticles",
            th.ArrayType(th.CustomType({"type": ["number", "string"]})),
        ),
        th.Property(
            "attachments", th.ArrayType(th.CustomType({"type": ["object", "string"]}))
        ),
        th.Property("type", th.StringType),
        th.Property("relationLists", th.ArrayType(th.NumberType)),
        th.Property(
            "customerGroupVariants", th.CustomType({"type": ["object", "string"]})
        ),
        th.Property("outOfStockMessage", th.CustomType({"type": ["object", "string"]})),
        th.Property("priceInquiryRequired", th.BooleanType),
        th.Property("numberSold", th.NumberType),
        th.Property("metaProperties", th.CustomType({"type": ["object", "string"]})),
    ).to_dict()


class ArticleGroupStream(AbicartStream):
    """Define custom stream."""

    name = "articlegroups"
    method = "Articlegroup.list"
    path = ""
    rest_method = "POST"
    primary_keys = ["uid"]
    replication_key = None
    schema = th.PropertiesList(
        th.Property("uid", th.NumberType),
        th.Property("name", th.CustomType({"type": ["object", "string"]})),
        th.Property("url", th.CustomType({"type": ["object", "string"]})),
        th.Property("baseName", th.CustomType({"type": ["object", "string"]})),
        th.Property("children", th.ArrayType(th.NumberType)),
        th.Property("hidden", th.BooleanType),
        th.Property("parent", th.NumberType),
        th.Property("description", th.CustomType({"type": ["object", "string"]})),
        th.Property("pageTitle", th.CustomType({"type": ["object", "string"]})),
        th.Property("metaDescription", th.CustomType({"type": ["object", "string"]})),
        th.Property("metaKeywords", th.CustomType({"type": ["object", "string"]})),
        th.Property("image", th.BooleanType),
        th.Property("pageItems", th.ArrayType(th.NumberType)),
    ).to_dict()


class SuppliersGroupStream(AbicartStream):
    """Define custom stream."""

    name = "suppliers"
    method = "Supplier.list"
    path = ""
    rest_method = "POST"
    primary_keys = ["uid"]
    replication_key = None
    schema = th.PropertiesList(
        th.Property("uid", th.NumberType),
        th.Property("company", th.StringType),
        th.Property("firstName", th.StringType),
        th.Property("lastName", th.StringType),
        th.Property("email", th.StringType),
        th.Property("orderEmail", th.StringType),
    ).to_dict()


class OrdersStream(AbicartStream):
    """Define custom stream."""

    name = "orders"
    method = "Order.list"
    path = ""
    rest_method = "POST"
    primary_keys = ["uid"]
    replication_key = "changed"
    schema = th.PropertiesList(
        th.Property("uid", th.NumberType),
        th.Property("orderNumber", th.StringType),
        th.Property("ordered", th.DateTimeType),
        th.Property("changed", th.DateTimeType),
        th.Property(
            "customer",
            th.ObjectType(
                th.Property(
                    "info",
                    th.ObjectType(
                        th.Property("type", th.StringType),
                        th.Property("email", th.StringType),
                        th.Property("nin", th.StringType),
                    ),
                ),
                th.Property(
                    "address",
                    th.ObjectType(
                        th.Property("firstName", th.StringType),
                        th.Property("lastName", th.StringType),
                        th.Property("address", th.StringType),
                        th.Property("postcode", th.StringType),
                        th.Property("town", th.StringType),
                        th.Property("country", th.StringType),
                        th.Property("phoneDay", th.StringType),
                    ),
                ),
                th.Property(
                    "deliveryAddress",
                    th.ObjectType(
                        th.Property("copyAddress", th.BooleanType),
                        th.Property("firstName", th.StringType),
                        th.Property("lastName", th.StringType),
                        th.Property("address", th.StringType),
                        th.Property("postcode", th.StringType),
                        th.Property("town", th.StringType),
                        th.Property("country", th.StringType),
                    ),
                ),
            ),
        ),
        th.Property("currency", th.StringType),
        th.Property("exchangeRate", th.NumberType),
        th.Property("items", th.ArrayType(th.NumberType)),
        th.Property(
            "costs",
            th.ObjectType(
                th.Property(
                    "total",
                    th.ObjectType(
                        th.Property("currency", th.StringType),
                        th.Property("exVat", th.NumberType),
                        th.Property("incVat", th.NumberType),
                        th.Property("vat", th.NumberType),
                        th.Property("vatRate", th.NumberType),
                    ),
                ),
                th.Property(
                    "cart",
                    th.ObjectType(
                        th.Property("currency", th.StringType),
                        th.Property("exVat", th.NumberType),
                        th.Property("incVat", th.NumberType),
                        th.Property("vat", th.NumberType),
                        th.Property("vatRate", th.NumberType),
                    ),
                ),
                th.Property(
                    "payment",
                    th.ObjectType(
                        th.Property("currency", th.StringType),
                        th.Property("exVat", th.NumberType),
                        th.Property("incVat", th.NumberType),
                        th.Property("vat", th.NumberType),
                        th.Property("vatRate", th.NumberType),
                    ),
                ),
                th.Property(
                    "shipment",
                    th.ObjectType(
                        th.Property("currency", th.StringType),
                        th.Property("exVat", th.NumberType),
                        th.Property("incVat", th.NumberType),
                        th.Property("vat", th.NumberType),
                        th.Property("vatRate", th.NumberType),
                    ),
                ),
            ),
        ),
        th.Property("language", th.StringType),
        th.Property("note", th.StringType),
        th.Property("tags", th.CustomType({"type": ["string", "object"]})),
        th.Property("externalOrderNumber", th.StringType),
        th.Property("discarded", th.BooleanType),
        th.Property("hasVat", th.BooleanType),
        th.Property("affiliate", th.NumberType),
        th.Property(
            "discountInfo", th.ArrayType(th.CustomType({"type": ["string", "object"]}))
        ),
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        if len(record["items"]):
            return {"items": record["items"]}
        else:
            return None

    def _sync_children(self, child_context: dict) -> None:
        for child_stream in self.child_streams:
            if child_stream.selected or child_stream.has_selected_descendents:
                if child_context is not None:
                    child_stream.sync(context=child_context)


class OrderItemStream(AbicartStream):
    """Define custom stream."""

    name = "orderitem"
    method = "OrderItem.get"
    path = ""
    rest_method = "POST"
    primary_keys = ["uid"]
    parent_stream_type = OrdersStream
    replication_key = None
    schema = th.PropertiesList(
        th.Property("uid", th.NumberType),
        th.Property("article", th.NumberType),
        th.Property("articleNumber", th.StringType),
        th.Property("choices", th.CustomType({"type": ["object"]})),
        th.Property(
            "costs",
            th.ObjectType(
                th.Property(
                    "total",
                    th.ObjectType(
                        th.Property("currency", th.StringType),
                        th.Property("exVat", th.NumberType),
                        th.Property("incVat", th.NumberType),
                        th.Property("vat", th.NumberType),
                        th.Property("vatRate", th.NumberType),
                    ),
                ),
                th.Property(
                    "unit",
                    th.ObjectType(
                        th.Property("currency", th.StringType),
                        th.Property("exVat", th.NumberType),
                        th.Property("incVat", th.NumberType),
                        th.Property("vat", th.NumberType),
                        th.Property("vatRate", th.NumberType),
                    ),
                ),
            ),
        ),
    ).to_dict()

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""
        items = self.items
        items_len = len(items)
        previous_token = previous_token or 0
        if items_len > 0 and previous_token < items_len - 1:
            next_page_token = previous_token + 1
            return next_page_token
        else:
            return None

    def prepare_request_payload(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Optional[dict]:
        """Prepare the data payload for the REST API request.
        By default, no payload will be sent (return None).
        """
        selected_properties = []
        self.items = context["items"]
        item_uid_index = next_page_token or 0
        item_uid = self.items[item_uid_index]

        for key, value in self.metadata.items():
            if isinstance(key, tuple) and len(key) == 2 and value.selected:
                field_name = key[-1]
                selected_properties.append(field_name)

        payload = {
            "jsonrpc": "2.0",
            "id": 3,
            "method": self.method,
            "params": [item_uid, selected_properties],
        }
        return payload


class WebshopStream(AbicartStream):
    """Define custom stream."""

    name = "webshop"
    method = "Webshop.list"
    path = ""
    rest_method = "POST"
    primary_keys = ["uid"]
    replication_key = None
    schema = th.PropertiesList(
        th.Property("uid", th.NumberType),
        th.Property("type", th.StringType),
        th.Property("edition", th.StringType),
        th.Property("loginRequiredToView", th.BooleanType),
        th.Property("companyInfo", th.CustomType({"type": ["object", "string"]})),
        th.Property("companyNumber", th.StringType),
        th.Property("created", th.DateTimeType),
        th.Property("name", th.CustomType({"type": ["object", "string"]})),
        th.Property("logotype", th.CustomType({"type": ["object", "string"]})),
        th.Property("expirationDate", th.DateTimeType),
        th.Property("category", th.StringType),
        th.Property("logolink", th.CustomType({"type": ["object", "string"]})),
        th.Property("favicon", th.StringType),
        th.Property("priceStoredWithVat", th.BooleanType),
        th.Property("vatSupport", th.BooleanType),
        th.Property("showPricesIncludingVat", th.BooleanType),
        th.Property("vatCountry", th.StringType),
        th.Property("url", th.CustomType({"type": ["object", "string"]})),
        th.Property("adminLanguage", th.StringType),
        th.Property("subscriptionInvoices", th.ArrayType(th.NumberType)),
        th.Property("baseCurrency", th.StringType),
        th.Property("activeCurrencies", th.ArrayType(th.StringType)),
        th.Property("defaultLanguage", th.StringType),
        th.Property("defaultDeliveryCountry", th.StringType),
        th.Property(
            "deliveryCountries",
            th.ArrayType(
                th.ObjectType(
                th.Property("code", th.StringType),
                th.Property("name", th.StringType),
            )),
        ),
        th.Property(
            "unsupportedDeliveryCountryText",
            th.CustomType({"type": ["object", "string"]}),
        ),
        th.Property("piwikId", th.StringType),
        th.Property("articlegroupPageTitleFormat", th.StringType),
        th.Property("articlePageTitleFormat", th.StringType),
        th.Property("customPagePageTitleFormat", th.StringType),
        th.Property("startPageTitle", th.CustomType({"type": ["object", "string"]})),
        th.Property(
            "startPageMetaDescription", th.CustomType({"type": ["object", "string"]})
        ),
        th.Property(
            "startPageMetaKeywords", th.CustomType({"type": ["object", "string"]})
        ),
        th.Property("contactEmail", th.StringType),
        th.Property("isShopSystem", th.BooleanType),
        th.Property("parent", th.NumberType),
        th.Property("minimalOrderCost", th.CustomType({"type": ["object", "string"]})),
        th.Property("checkoutNewsletter", th.StringType),
        th.Property(
            "orderEmailSettings", th.CustomType({"type": ["object", "string"]})
        ),
        th.Property(
            "textAfterCheckoutComplete", th.CustomType({"type": ["object", "string"]})
        ),
        th.Property("allowCustomerRegistration", th.BooleanType),
        th.Property("customerLoginEnabled", th.BooleanType),
        th.Property("stockSubscriptionEnabled", th.BooleanType),
        th.Property(
            "checkoutIntroductionText", th.CustomType({"type": ["object", "string"]})
        ),
        th.Property("defaultCustomerType", th.StringType),
        th.Property("hasCustomergroupAssortment", th.BooleanType),
        th.Property("danads", th.CustomType({"type": ["object", "string"]})),
        th.Property(
            "publicSocialMediaParams", th.CustomType({"type": ["object", "string"]})
        ),
        th.Property(
            "privateSocialMediaParams", th.CustomType({"type": ["object", "string"]})
        ),
    ).to_dict()
