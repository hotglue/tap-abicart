"""REST client handling, including AbicartStream base class."""

from typing import Any, Dict, Optional, Generator, Callable

import requests
from pendulum import parse
from singer_sdk.streams import RESTStream
import backoff
from singer_sdk.exceptions import  RetriableAPIError
from tap_abicart.auth import AbicartAuthenticator


class AbicartStream(RESTStream):
    """Abicart stream class."""

    limit = 500
    access_token = None

    @property
    def url_base(self) -> str:
        """Return the API URL root, configurable via tap settings."""
        webshop = self.config["webshop"]
        base_url = f"https://admin.abicart.se/backend/jsonrpc/v1?webshop={webshop}"
        return base_url

    records_jsonpath = "$.result[*]"

    @property
    def authenticator(self) -> AbicartAuthenticator:
        """Return a new authenticator object."""
        return AbicartAuthenticator(
            self, self._tap.config, "https://admin.abicart.se/backend/jsonrpc/v1"
        )
    
    def refresh_access_token(self):
        self.access_token = self.authenticator.update_access_token()

    @property
    def http_headers(self) -> dict:
        """Return the http headers needed."""
        headers = {}
        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent")
        return headers

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""
        previous_token = previous_token or 0
        response_json = response.json()
        if "result" not in response_json:
            return None
        if len(response_json["result"]):
            next_page_token = self.limit + previous_token
        else:
            next_page_token = None
        return next_page_token

    def get_starting_time(self, context):
        start_date = self.config.get("start_date")
        if start_date:
            start_date = parse(self.config.get("start_date"))
        rep_key = self.get_starting_timestamp(context)
        return rep_key or start_date

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        params: dict = {}
        if self.access_token is None:
            self.refresh_access_token()
        access_token = self.access_token
        if access_token:
            params["auth"] = access_token
        return params
    
    def backoff_wait_generator(self) -> Generator[float, None, None]:
        return backoff.expo(base=2,factor=3) 
    
    def backoff_max_tries(self) -> int:
        return 7

    def prepare_request_payload(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Optional[dict]:
        """Prepare the data payload for the REST API request.
        By default, no payload will be sent (return None).
        """
        selected_properties = []
        for key, value in self.metadata.items():
            if isinstance(key, tuple) and len(key) == 2 and value.selected:
                field_name = key[-1]
                selected_properties.append(field_name)

        payload = {
            "jsonrpc": "2.0",
            "id": 3,
            "method": self.method,
            "params": [
                selected_properties,
                {"filters": {}, "limit": self.limit, "offset": next_page_token or 0},
            ],
        }

        if self.replication_key:
            start_date = self.get_starting_time(context)
            start_date = start_date.strftime("%Y-%m-%dT%H:%M:%SZ")
            if start_date:
                if len(payload['params'])>1:
                    payload['params'][1]["filters"].update({"/changed": {"min": start_date}})
                #Highly unlikely because of JSON-RPC. But, uncomment this line just incase other streams need it.  
                # payload["filters"] = {"/changed": {"min": start_date}}    

        return payload
    
    def request_decorator(self, func: Callable) -> Callable:
        """Instantiate a decorator for handling request failures."""
        decorator: Callable = backoff.on_exception(
            backoff.expo,
            (
                RetriableAPIError,
                requests.exceptions.ReadTimeout,
                requests.exceptions.ConnectionError,
                requests.exceptions.ChunkedEncodingError,
            ),
            max_tries=8,
            factor=2,
        )(func)
        return decorator

